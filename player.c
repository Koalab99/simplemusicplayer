#include <gst/gst.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <strings.h>
#include <stdio.h>

#define BUFFER_SIZE 512

static int argc;
static char** argv;

void printUsage() {
	printf("Usage: %s < PATH >\n\tPATH: \
		Absolute path of a music file\n\n", argv[0]);
}

int main (int argcd, char *argvd[])
{
	argc = argcd;
	argv = argvd;
	if(argc != 2) {
		printUsage();
		exit(EXIT_FAILURE);
	}
	if(strlen(argv[1]) > BUFFER_SIZE-24) {
		printf("Filename too long to fit into buffer");
		exit(EXIT_FAILURE);
	}

	printf("\"%s\"\n", argv[1]);

	GstElement *pipeline;
	GstBus *bus;
	GstMessage *msg;

	char *command = malloc(sizeof(char) * BUFFER_SIZE);
	strcpy(command, "playbin uri=\"file://");
	strcat(command, argv[1]);
	strcat(command, "\"");

	/* Initialize GStreamer */
	gst_init (&argc, &argv);

	/* Build the pipeline */
	pipeline = gst_parse_launch(command, NULL);

	/* Start playing */
	gst_element_set_state (pipeline, GST_STATE_PLAYING);

	/* Wait until error or EOS */
	bus = gst_element_get_bus (pipeline);
	msg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE,
				GST_MESSAGE_ERROR | GST_MESSAGE_EOS);

	/* Free resources */
	if (msg != NULL)
		gst_message_unref (msg);
	gst_object_unref (bus);
	gst_element_set_state (pipeline, GST_STATE_NULL);
	gst_object_unref (pipeline);
	return 0;
}


