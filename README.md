Most of this code is not from me.
I just took the source code [from the gstreamer tutorial](https://gstreamer.freedesktop.org/documentation/tutorials/basic/hello-world.html?gi-language=c) and adapted it to my need.

# SimpleMusicPlayer
Just a Simple Music Player based on gstreamer library.

## Dependancies
### Functional dependancies
* gstreamer-1.0
### Optional dependancies
* gst-plugins-base
* gst-plugins-good
* gst-plugins-ugly
* gst-plugins-bad
* gst-libav

## How to use
First you need to compile the source file with
```
make
```
Then you can play song with
```
./player ~/Music/myFavoriteSong.ogg
```
