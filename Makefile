FLAGS=`pkg-config --libs --cflags gstreamer-1.0`

all: player

player: player.c
	gcc player.c -o player $(FLAGS)

clean:
	rm player
